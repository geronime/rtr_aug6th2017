#include<windows.h>
#include<GL/gl.h>
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "opengl32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global Variable declaration
HDC ghdc;
HGLRC ghglrc;
HWND ghwnd;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbFullscreen = false;
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;

int giFlag = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	void Initialize(void);
	void display();
	void Uninitialize(void);
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClsName[] = TEXT("GL_TRIANGLES");
	bool bDone = false;

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbWndExtra = 0;
	wndClass.cbClsExtra = 0;
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szClsName;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

	if (!RegisterClassEx(&wndClass)) {
		MessageBox(NULL, TEXT("Class Not Register Successfully\n Exiting.."), TEXT("ERROR"), MB_ICONERROR);
		exit(0);
	}

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClsName,
		TEXT("GL_TRIANGLES"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if (hwnd == NULL) {
		MessageBox(NULL, TEXT("Failed To Create Window \n Exiting.."), TEXT("ERROR"), MB_ICONERROR);
		exit(0);
	}

	ghwnd = hwnd;

	Initialize();
	ShowWindow(hwnd, nCmdShow);

	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT)
				bDone = true;
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow == true) {
				if (gbEscapeKeyIsPressed == true)
					bDone = true;

				display();
			}
		}
	}

	Uninitialize();
	return((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	void MakeFullscreen(void);
	void LeaveFullscreen(void);
	void Uninitialize(void);
	void display(void);
	void resize(int, int);

	switch (iMsg) {
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		switch (wParam) {
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;

		case 0x46: // 'f' OR 'F'
			if (gbFullscreen == false) {
				MakeFullscreen();
				gbFullscreen = true;
			}
			else {
				LeaveFullscreen();
				gbFullscreen = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_CLOSE:
		Uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void Initialize(void) {
	void resize(int, int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormat = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);
	iPixelFormat = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormat == 0) {
		MessageBox(ghwnd, TEXT("Failed To Choose Pixel Format"), TEXT("ERROR"), MB_ICONERROR);
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormat, &pfd) == FALSE) {
		MessageBox(ghwnd, TEXT("Failed To Set Pixel Format"), TEXT("ERROR"), MB_ICONERROR);
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if ((ghglrc = wglCreateContext(ghdc)) == NULL) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghglrc) == FALSE) {
		wglDeleteContext(ghglrc);
		ghglrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	//resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT);

	static GLfloat glfI1X1 = -1.8f, glfI1X2 = -1.79f, glfI1X3 = -1.79f, glfI1X4 = -1.8f;
	static GLfloat glfI1Y1 = 0.8f, glfI1Y2 = 0.8f, glfI1Y3 = -0.8f, glfI1Y4 = -0.8f;

	static GLfloat glfNX1 = -0.6f, glfNX2 = -0.59f, glfNX3 = -0.59f, glfNX4 = -0.6f, glfNX5 = -0.6f, glfNX6 = -0.59f;
	static GLfloat glfNX7 = -0.29f, glfNX8 = -0.3f, glfNX9 = -0.3f, glfNX10 = -0.29f, glfNX11 = -0.29f, glfNX12 = -0.3f;
	static GLfloat glfNY1 = 2.8f, glfNY2 = 2.8f, glfNY3 = 1.8f, glfNY4 = 1.8f, glfNY5 = 2.8f, glfNY6 = 2.8f;
	static GLfloat glfNY7 = 1.8f, glfNY8 = 1.8f, glfNY9 = 2.8f, glfNY10 = 2.8f, glfNY11 = 1.8f, glfNY12 = 1.8f;

	static GLfloat glfDX1 = -0.1f, glfDX2 = -0.09f, glfDX3 = -0.09f, glfDX4 = -0.1f, glfDX5 = -0.15f, glfDX6 = -0.15f;
	static GLfloat glfDX7 = 0.2f, glfDX8 = 0.2f, glfDX9 = 0.2f, glfDX10 = 0.19f, glfDX11 = 0.19f, glfDX12 = 0.2f;
	static GLfloat glfDX13 = -0.15f, glfDX14 = -0.15, glfDX15 = 0.2f, glfDX16 = 0.2f;
	static GLfloat glfDY1 = 0.8f, glfDY2 = 0.8f, glfDY3 = -0.8f, glfDY4 = -0.8f, glfDY5 = 0.785f, glfDY6 = 0.8f;
	static GLfloat glfDY7 = 0.8f, glfDY8 = 0.785f, glfDY9 = 0.8f, glfDY10 = 0.8f, glfDY11 = -0.8f, glfDY12 = -0.8f;
	static GLfloat glfDY13 = -0.785f, glfDY14 = -0.8, glfDY15 = -0.8f, glfDY16 = -0.785f;

	static GLfloat glfI2X1 = 0.4f, glfI2X2 = 0.39f, glfI2X3 = 0.39f, glfI2X4 = 0.4f;
	static GLfloat glfI2Y1 = -1.8f, glfI2Y2 = -1.8f, glfI2Y3 = -2.8f, glfI2Y4 = -2.8f;

	static GLfloat glfAX1 = 1.7f, glfAX2 = 1.69f, glfAX3 = 1.49f, glfAX4 = 1.5f, glfAX5 = 1.7f, glfAX6 = 1.69f;
	static GLfloat glfAX7 = 1.89f, glfAX8 = 1.9f, glfAX9 = -1.6f, glfAX10 = -1.79f, glfAX11 = -1.598f, glfAX12 = -1.79f;
	static GLfloat glfAX13 = -1.595f, glfAX14 = -1.795;
	static GLfloat glfAY1 = 0.8f, glfAY2 = 0.8f, glfAY3 = -0.8f, glfAY4 = -0.8f, glfAY5 = 0.8f, glfAY6 = 0.8f;
	static GLfloat glfAY7 = -0.8f, glfAY8 = -0.8f, glfAY9 = 0.0f, glfAY10 = 0.0f, glfAY11 = -0.009f, glfAY12 = -0.009f;
	static GLfloat glfAY13 = -0.018f, glfAY14 = -0.018;

	

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();	
	glLineWidth(1.0);
	// 'I'
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfI1X1, glfI1Y1, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfI1X2, glfI1Y2, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfI1X3, glfI1Y3, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfI1X4, glfI1Y4, 0.0f);
	glEnd();

	
	if (glfI1X1 <= -0.8f) {
		glfI1X1 += 0.00008f;
		glfI1X2 += 0.00008f;
		glfI1X3 += 0.00008f;
		glfI1X4 += 0.00008f;
	}
	if (glfI1X1 >= -0.8)
		giFlag = 1;			

	// 'N'
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfNX1, glfNY1, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfNX2, glfNY2, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfNX3, glfNY3, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfNX4, glfNY4, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfNX5, glfNY5, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfNX6, glfNY6, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfNX7, glfNY7, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfNX8, glfNY8, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfNX9, glfNY9, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfNX10, glfNY10, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfNX11, glfNY11, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfNX12, glfNY12, 0.0f);
	glEnd();

	if (glfNY1 >= 0.8f && giFlag == 1) {
		glfNY1 -= 0.00008f;
		glfNY2 -= 0.00008f;
		glfNY5 -= 0.00008f;
		glfNY6 -= 0.00008f;
		glfNY9 -= 0.00008f;
		glfNY10 -= 0.00008f;

		glfNY3 -= 0.000104f;
		glfNY4 -= 0.000104f;
		glfNY7 -= 0.000104f;
		glfNY8 -= 0.000104f;
		glfNY11 -= 0.000104f;
		glfNY12 -= 0.000104f;
	}
	if (glfNY1 <= 0.8)
		giFlag = 2;

	static GLfloat colorRed = 0.0f, colorGreen = 0.0f;

	// 'D'
	glBegin(GL_QUADS);
	glColor3f(colorRed, colorGreen, 0.0f);
	glVertex3f(glfDX1, glfDY1, 0.0f);
	glColor3f(colorRed, colorGreen, 0.0f);
	glVertex3f(glfDX2, glfDY2, 0.0f);
	glColor3f(0.0f, colorGreen, 0.0f);
	glVertex3f(glfDX3, glfDY3, 0.0f);
	glColor3f(0.0f, colorGreen, 0.0f);
	glVertex3f(glfDX4, glfDY4, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(colorRed, colorGreen, 0.0f);
	glVertex3f(glfDX5, glfDY5, 0.0f);
	glVertex3f(glfDX6, glfDY6, 0.0f);
	glVertex3f(glfDX7, glfDY7, 0.0f);
	glVertex3f(glfDX8, glfDY8, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(colorRed, colorGreen, 0.0f);
	glVertex3f(glfDX9, glfDY9, 0.0f);
	glColor3f(colorRed, colorGreen, 0.0f);
	glVertex3f(glfDX10, glfDY10, 0.0f);
	glColor3f(0.0f, colorGreen, 0.0f);
	glVertex3f(glfDX11, glfDY11, 0.0f);
	glColor3f(0.0f, colorGreen, 0.0f);
	glVertex3f(glfDX12, glfDY12, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, colorGreen, 0.0f);
	glVertex3f(glfDX13, glfDY13, 0.0f);
	glVertex3f(glfDX14, glfDY14, 0.0f);
	glVertex3f(glfDX15, glfDY15, 0.0f);
	glVertex3f(glfDX16, glfDY16, 0.0f);
	glEnd();

	if (colorRed <= 1.0f && giFlag == 2) {
		colorRed += 0.00004f;
		colorGreen += 0.00002f;
	}
	if (colorRed >= 1.0f)
		giFlag = 3;

	// 'I'
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfI2X1, glfI2Y1, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfI2X2, glfI2Y2, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfI2X3, glfI2Y3, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfI2X4, glfI2Y4, 0.0f);
	glEnd();

	if (glfI2Y1 <= 0.8f && giFlag == 3) {
		glfI2Y1 += 0.00008f;
		glfI2Y2 += 0.00008f;
		glfI2Y3 += 0.0000611f;
		glfI2Y4 += 0.0000611f;
	}
	if (glfI2Y1 >= 0.8f)
		giFlag = 4;

	// 'A'
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfAX1, glfAY1, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfAX2, glfAY2, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfAX3, glfAY3, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfAX4, glfAY4, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfAX5, glfAY5, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfAX6, glfAY6, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfAX7, glfAY7, 0.0f);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfAX8, glfAY8, 0.0f);
	glEnd();

	glLineWidth(10.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(glfAX9, glfAY9, 0.0f);
	glVertex3f(glfAX10, glfAY10, 0.0f);
	glEnd();

	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(glfAX11, glfAY11, 0.0f);
	glVertex3f(glfAX12, glfAY12, 0.0f);
	glEnd();

	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(glfAX13, glfAY13, 0.0f);
	glVertex3f(glfAX14, glfAY14, 0.0f);
	glEnd();

	if (glfAX1 >= 0.7f && giFlag == 4) {
		glfAX1 -= 0.00008f;
		glfAX2 -= 0.00008f;
		glfAX3 -= 0.00008f;
		glfAX4 -= 0.00008f;
		glfAX5 -= 0.00008f;
		glfAX6 -= 0.00008f;
		glfAX7 -= 0.00008f;
		glfAX8 -= 0.00008f;
	}
	if (glfAX1 <= 0.7f)
		giFlag = 5;

	if (glfAX14 <= 0.795f && giFlag == 5) {
		glfAX10 += 0.00008f;
		glfAX12 += 0.00008f;
		glfAX14 += 0.00008f;		
	}
	if (glfAX10 >= 0.79f)
		giFlag = 6;

	if (glfAX13 <= 0.595f && giFlag == 6) {
		glfAX9 += 0.00008f;
		glfAX11 += 0.00008f;
		glfAX13 += 0.00008f;
	}

	SwapBuffers(ghdc);
}

void resize(int iWidth, int iHeight) {
	if (iHeight == 0)
		iHeight = 1;

	glViewport(0, 0, (GLsizei)iWidth, (GLsizei)iHeight);
}

void MakeFullscreen(void) {
	MONITORINFO mi = { sizeof(MONITORINFO) };

	dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

	if (dwStyle & WS_OVERLAPPEDWINDOW) {
		if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITOR_DEFAULTTOPRIMARY), &mi)) {
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
		}
	}
	ShowCursor(FALSE);
}

void LeaveFullscreen(void) {
	SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
	SetWindowPlacement(ghwnd, &wpPrev);
	SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
	ShowCursor(TRUE);
}

void Uninitialize(void) {
	if (gbFullscreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghglrc);
	ghglrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
}